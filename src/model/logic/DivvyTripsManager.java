package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoublyLinkedList<String[]> trips;

	private DoublyLinkedList<String[]> stations;


	public void loadStations (String stationsFile) {
		try 
		{
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String[] info = reader.readNext();
			stations = new DoublyLinkedList<String[]>(info);
			while ((info = reader.readNext())!=null)
			{
				Node<String[]> nuevo = new Node<String[]>(info, null, null);
				stations.addAtBeginning(nuevo);;
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}


	public void loadTrips (String tripsFile) {
		try 
		{
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String[] info = reader.readNext();
			trips = new DoublyLinkedList<String[]>(info);
			while ((info = reader.readNext())!=null)
			{
				Node<String[]> nuevo = new Node<String[]>(info, null, null);
				trips.addAtEnd(nuevo);
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		Node<String[]> element = trips.getFirstElement().getNext();
		int id = 0;
		double tripSeconds = 0.0;
		String originStation = "";
		String destinationStation = "";
		DoublyLinkedList<VOTrip> tripsConGenero = new DoublyLinkedList<>();
		while (element!=null)
		{
			if(element.getInfo()[10].equals(gender))
			{
				id = Integer.valueOf(element.getInfo()[0]);
				tripSeconds = Double.valueOf(element.getInfo()[4]);
				originStation = element.getInfo()[6];
				destinationStation = element.getInfo()[8];
				Node<VOTrip> nuevoTrip = new Node<VOTrip>(new VOTrip(id, tripSeconds, originStation, destinationStation), null, null);
				tripsConGenero.addAtBeginning(nuevoTrip);
			}
			element = element.getNext();
		}
		return tripsConGenero;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		Node<String[]> element = trips.getFirstElement().getNext();
		int id = 0;
		double tripSeconds = 0.0;
		String originStation = "";
		String destinationStation = "";
		DoublyLinkedList<VOTrip> tripsHasta = new DoublyLinkedList<>();
		while (element!=null)
		{
			if(element.getInfo()[7].equals(stationID))
			{
				id = Integer.valueOf(element.getInfo()[0]);
				tripSeconds = Double.valueOf(element.getInfo()[4]);
				originStation = element.getInfo()[6];
				destinationStation = element.getInfo()[8];
				Node<VOTrip> nuevoTrip = new Node<VOTrip>(new VOTrip(id, tripSeconds, originStation, destinationStation), null, null);
				tripsHasta.addAtBeginning(nuevoTrip);
			}
			element = element.getNext();
		}
		return tripsHasta;	
		}	 
}
