package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	//Integer getSize();
		
	public void addAtBeginning(Node<T> elemento);
	
	public void addAtEnd(Node<T> elemento);
	
	public void addAtK(Node<T> elemento, int k);
	
	public void delete(Node<T> elemento);
	
	public void deleteAtK(int k);
	
	public int getSize();
	
	public T getElement();
	
	public Node<T> startRoute(T wanted);
	
	public Node<T> getCurrentElement();
	
	public Node<T> nextElement();
	
	public Node<T> previousElement();
	

}
