package model.data_structures;

import java.util.Iterator;
import java.util.function.DoubleUnaryOperator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T>{

	private Node<T> elemento;
	private Node<T> first;
	private Node<T> last;


	public DoublyLinkedList(T elemento) {
		first = new Node<T>(elemento, null, null);
		last = first;
	}

	public DoublyLinkedList(Node<T> elemento) {
		elemento.setNext(null);
		elemento.setPrevious(null);
		first = elemento;
		last = elemento;
	}

	public DoublyLinkedList() {
		first = null;
		last = null;
	}

	@Override
	public void addAtBeginning(Node<T> elemento) {
		Node<T> temp = first;
		if(temp == null)
		{
			first = elemento;
			last = elemento;
		}
		else
		{
			first = elemento;
			first.setNext(temp);
			temp.setPrevious(first);
		}
	}

	@Override
	public T getElement() {
		return elemento.getInfo();
	}

	@Override
	public Node<T> startRoute(T wanted) {
		elemento = first;
		if(elemento.getInfo() == wanted)
			return elemento;
		while(elemento.getNext()!=null)
		{
			elemento = elemento.getNext();
			if (elemento.getInfo() == wanted)
				return elemento;
		}
		return null;
	}

	public Node<T> getFirstElement()
	{
		return first;
	}

	@Override
	public Node<T> getCurrentElement() {
		return elemento;
	}

	@Override
	public Node<T> nextElement() {
		return elemento.getNext();
	}

	@Override
	public Node<T> previousElement() {
		return elemento.getPrevious();
	}


	@Override
	public void addAtEnd(Node<T> elemento) {
		if(last==null && first==null)
		{
			first = elemento;
			last = elemento;
		}
		last.setNext(elemento);
		elemento.setPrevious(last);
		last = elemento;
	}

	@Override
	//lo agrega antes del elemento que ocupa la posicion k en la lista, empezando a contar desde k
	public void addAtK(Node<T> nuevo, int k) {
		// TODO Auto-generated method stub
		if(nuevo!=null){
			int cont = 0;
			elemento = first;
			if(elemento == null)
			{
				elemento.setNext(null);
				elemento.setPrevious(null);
				first = elemento;
				last = elemento;
			}
			while (elemento.getNext()!=null && cont<k)
			{
				elemento = elemento.getNext();
				cont ++;
			}
			if(elemento.getNext()==null)
				addAtEnd(nuevo);
			if(k==cont)
			{
				Node<T> anterior = elemento.getPrevious();
				if(anterior == null)
				{
					addAtBeginning(nuevo);
				}
				anterior.setNext(nuevo);
				nuevo.setPrevious(anterior);
				nuevo.setNext(elemento);
				elemento.setPrevious(nuevo);
			}
		}
	}


	@Override
	public void delete(Node<T> ugly) {
		Node<T> next = ugly.getNext();
		Node<T> previous = ugly.getPrevious();
		if(next==null && previous==null){
			ugly.setNext(null);
			ugly.setPrevious(null);
			first = ugly;
			last = ugly;
		}
		else if(next==null){
			previous.setNext(null);
			last= previous;
		}
		else if (previous==null)
		{
			next.setPrevious(null);
			first = previous;
		}
		else 
		{
			previous.setNext(next);
			next.setPrevious(previous);
		}
	}

	@Override
	public void deleteAtK( int k) {
		elemento = first;
		if(elemento!=null){
			int cont = 0;
			while (elemento.getNext()!=null && cont<k)
			{
				elemento = elemento.getNext();
				cont++;
			}
			if(k==cont)
			{
				delete(elemento);
			}
		}

	}

	@Override
	public int getSize() {
		elemento = first;
		int cont=0;
		if(elemento ==null)
			return 0;
		if(elemento.getNext() ==null)
			return 1;
		while(elemento.getNext()!=null)
		{
			elemento = elemento.getNext();
			cont ++;
		}
		return cont;
	}

	@Override
	public Iterator<T> iterator() {

		return null;
	}

}
