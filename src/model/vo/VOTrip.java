package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	private double tripSeconds;
	private String originStation;
	private String destinationStation;
	/*
	public VOTrip(int id, String start, String end, String originStation, String destinationStation)
	{
		this.id = id;
		try 
		{
			Date dateStart = new SimpleDateFormat("MM/dd/yyyy HH:mm").parse(start);
			Date dateEnd = new SimpleDateFormat("MM/dd/yyyy HH:mm").parse(end);
			tripSeconds = (dateEnd.getTime()-dateStart.getTime())/1000;
		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.originStation = originStation;
		this.destinationStation = destinationStation;
	}
	*/
	
	public VOTrip(int id, double tripSeconds, String originStation, String destinationStation)
	{
		this.id = id;
		this.tripSeconds = tripSeconds;
		this.originStation = originStation;
		this.destinationStation = destinationStation;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getOriginStation() {
		return originStation;
	}


	public void setOriginStation(String originStation) {
		this.originStation = originStation;
	}


	public String getDestinationStation() {
		return destinationStation;
	}


	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}


	public double getTripSeconds() {
		return tripSeconds;
	}


	public void setTripSeconds(double tripSeconds) 
	{
		this.tripSeconds = tripSeconds;
	}
}
